<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="fr">
<head>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/Style.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<script type="text/javascript" src="https://echarts.apache.org/examples/vendors/echarts/echarts.min.js"></script>
</head>
	<body>
		<div id="main" style="width: 800px; height:800px;display:block">

		</div>
		<script src="<?php echo base_url(); ?>assets/js/main.js"></script>
	</body>
</html>