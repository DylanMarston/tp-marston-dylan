<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_test extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}



	public function select_all()
	{
		$query = $this->db->select("idMachine, type")
						  ->from("machine")
						  ->get();

		return $query->result_array(); 
	}
}
