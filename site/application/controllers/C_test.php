<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_test extends CI_Controller {

	public function index()
	{
		
		$this->load->model('M_test');

		$data["result"] = $this->M_test->select_all();

		

		$this->load->view("V_test",$data);

	}
}
