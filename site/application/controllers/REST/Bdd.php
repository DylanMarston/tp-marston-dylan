<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

    class Bdd extends REST_Controller{

        public  function __construct() {

            parent::__construct();
            $this->load->model('M_test');
            $this->load->helper("url"); 
            
        }

        public function index_get($id = ''){

            if($id === ''){

                $results = $this->M_test->select_all();
                $this->response($results, REST_Controller::HTTP_OK);

            }

        }

    }
